package com.perfm.services.userServices;

import com.perfm.services.model.EmployeeDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

public class EmployeeDetailServiceImpl implements EmployeeDetailService{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<EmployeeDetail> getAllEmployeeDetail() {
        return mongoTemplate.findAll(EmployeeDetail.class);
    }

    @Override
    public EmployeeDetail getEmployeeDetailByEmployeeId(String employeeId) {
        return null;
    }
}
