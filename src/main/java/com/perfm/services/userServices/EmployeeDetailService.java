package com.perfm.services.userServices;

import com.perfm.services.model.EmployeeDetail;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeDetailService {

    List<EmployeeDetail> getAllEmployeeDetail();

    EmployeeDetail getEmployeeDetailByEmployeeId(String employeeId);

}
